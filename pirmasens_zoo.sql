/*Leg1 */
DROP TABLE if EXISTS Vertretung_MA;
CREATE TABLE Vertretung_MA(
									Vertretung_MA_id 										INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Vertreter															INTEGER,
									Vertretender													INTEGER,
									Vertretungsgrund											VARCHAR (30),
									Von											    					date,
									Bis																		date,
									Bemerkungen													VARCHAR (250),	
								   FOREIGN KEY (Vertretender) 			    REFERENCES Mitarbeiter(Mitarbeiter_id)
								   FOREIGN KEY (Vertreter) 						REFERENCES Mitarbeiter(Mitarbeiter_id)	
);


DROP TABLE if EXISTS Mitarbeiter;
CREATE TABLE Mitarbeiter(
									Mitarbeiter_id     					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Funktion									INTEGER,
									Anrede										VARCHAR (10),
									Nachname  								VARCHAR (30),
									Vorname  							    VARCHAR (30),
									Strasse  									VARCHAR (50),
									Hausnummer 						VARCHAR (10),  
									Postleitzahl								VARCHAR (5),  
									Ort												VARCHAR (50),
									Telefonnummer 					VARCHAR (20), 
									eMail 					   					VARCHAR (50),
									seit												date
);


DROP TABLE if EXISTS TierMitarbeiter;
CREATE TABLE TierMitarbeiter(
									TierMitarbeiter_id									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Mitarbeiter_id 											INTEGER NOT NULL,
									Tier_id															INTEGER NOT NULL,
									seit																	date,
									FOREIGN KEY (Mitarbeiter_id) 		REFERENCES Mitarbeiter(Mitarbeiter_id)
									FOREIGN KEY (Tier_id) 						REFERENCES Tier(Tier_id)
);

DROP TABLE if EXISTS TierartMitarbeiter;
CREATE TABLE TierartMitarbeiter(
									TierartMitarbeiter_id								INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Mitarbeiter_id 											INTEGER,
									Tierart_id													INTEGER,
									seit																	date,
									FOREIGN KEY (Mitarbeiter_id)     	REFERENCES Mitarbeiter(Mitarbeiter_id)
									FOREIGN KEY (Tierart_id) 					REFERENCES Tierart(Tierart_id)
);

DROP TABLE if EXISTS Tierart;
CREATE TABLE Tierart(
									Tierart_id									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Gattung_id 									INTEGER,
									Mitarbeiter_id							INTEGER,
									Natuerlicher_Lebensraum		VARCHAR (80),
									Verhaltenweisen					    VARCHAR (80),
									Abstammung								VARCHAR (80),
									Verwandten_Arten				    VARCHAR (80),
									Bemerkungen							   	VARCHAR (250),
									Bezeichnung							  	VARCHAR (250),
									FOREIGN KEY  (Mitarbeiter_id)    REFERENCES Mitarbeiter(Mitarbeiter_id)
									FOREIGN KEY  (Gattung_id)			REFERENCES Gattung(Gattung_id)
);


/*Leg2 */
DROP TABLE if EXISTS Gattung;
CREATE TABLE Gattung(
									Gattung_id 									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Bezeichung									VARCHAR (100),   
									Generelle_Informationen		VARCHAR (250),
									Bermerkungen							VARCHAR (250)
);

/*Leg5 */
DROP TABLE if EXISTS Lieferant;
CREATE TABLE Lieferant(
									Lieferant_id    						   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Firmenname							VARCHAR (60),  
									Anrede										VARCHAR (8),
									Nachname  								VARCHAR (30),
									Vorname  								VARCHAR (30),
									Ansprechepartner_Feld1   VARCHAR (50),
									Ansprechepartner_Feld2   VARCHAR (50),
									Strasse  									VARCHAR (50),
									Hausnummer 						VARCHAR (10), 
									Postleitzahl								VARCHAR (5),
									Ort												VARCHAR (50),
									eMail 					   					VARCHAR (50),								
									Telefonnummer 					VARCHAR (20),			
									Fax							 					VARCHAR (20)
);


DROP TABLE if EXISTS FutterLieferant;
CREATE TABLE FutterLieferant(
									FutterLieferant_id   			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Futter_id								INTEGER,
									Lieferant_id							INTEGER,
									Bestelldatum 			    		Date,
									Mengeneinheit					VARCHAR (20),
									Bestellmenge						REAL,
									Preis_je_Einheit_netto  	REAL,
									Gewaehrter_Rabatt			REAL,
									Lieferdatum 			    		Date,
									FOREIGN KEY (Futter_id) 		REFERENCES Futter
									FOREIGN KEY (Lieferant_id)  REFERENCES Lieferant
);

DROP TABLE if EXISTS  Fuetterungskalender;
CREATE TABLE Fuetterungskalender(
									Fuetterungs_id  				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Futter_id							INTEGER,
									Tier_id								INTEGER,
									Datum								Date,
									Uhrzeit								TIME,
									Menge								REAL,
									Mengeneinheit				VARCHAR (20),
									Gefuettert						VARCHAR (8),
									FOREIGN KEY (Futter_id) REFERENCES Futter(Futter_id)
									FOREIGN KEY (Tier_id)     REFERENCES	Tier(Tier_id)
);

DROP TABLE if EXISTS Futter;
CREATE TABLE Futter(
									 Futter_id  						INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Futterart_id					INTEGER,
									Bezeichnung					VARCHAR (50),    
									Eiserner_Bestand			REAL,
									Lagerbestand					REAL,
									Bestellbestand				REAL,		
									Offene_Bestellmenge	REAL,							 
									Mengeneinheit				VARCHAR (20),
									Lagerort							VARCHAR (30),
									FOREIGN KEY (Futterart_id) REFERENCES Futterart(Futterart_id)
);
				
/*Leg4 */	
DROP TABLE if EXISTS Futterart;
CREATE TABLE Futterart(
									Futterart_id   			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Futterart				VARCHAR (90)
);
					


DROP TABLE if EXISTS Krankengeschichte;
CREATE TABLE Krankengeschichte(
					Krankengeschichte_id     			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
					Tier_id												INTEGER,
					Tierarzt_id										INTEGER,
					Behandlungsgrund						VARCHAR (200),
		            Meldepflicht  			                		VARCHAR (8),
					Krankheit										VARCHAR (50),
					Datum												date,
					Befund												VARCHAR (250),		
					Medikation										VARCHAR (70),
					Bemerkungen									VARCHAR (250),
					Behandlung_abgeschlossen 		VARCHAR (8),  
					FOREIGN KEY (Tierarzt_id)      REFERENCES Tierarzt(Tierarzt_id)
					FOREIGN KEY (Tier_id)          	  REFERENCES Tier(Tier_id)
);

/*Leg3 */	
DROP TABLE if EXISTS Vertretung_TA;
CREATE TABLE Vertretung_TA(
									Vertretung_TA_id 						INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Vertreter										INTEGER,
									Vertretender								INTEGER,
									Vertretungsgrund						VARCHAR (30),
									Von											    date,
									Bis													date,
									Bemerkungen								VARCHAR (250),
									FOREIGN KEY (Vertreter)           REFERENCES Tierarzt(Tierarzt_id)
									FOREIGN KEY (Vertretender)     REFERENCES Tierarzt(Tierarzt_id)
);

DROP TABLE if EXISTS Tierarzt;
CREATE TABLE Tierarzt(
									Tierarzt_id     				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Anrede							VARCHAR (8),   
									Titel								VARCHAR (20),
									Praxisname					VARCHAR (50),
									Nachname  					VARCHAR (30),
									Vorname  				    VARCHAR (30),
									Strasse  						VARCHAR (50),
									Hausnummer 			VARCHAR (10),   
									Postleitzahl					VARCHAR (5),
									Ort									VARCHAR (50),
									Telefonnummer 		VARCHAR (20), 
									Fax							 		VARCHAR (20), 
									eMail 					   		VARCHAR (50),
									Bemerkungen				VARCHAR (250)
									
);


DROP TABLE if EXISTS Unterbringung;
CREATE TABLE Unterbringung(
									Unterbringung_id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Gehege_id					INTEGER,
									Tier_id							INTEGER,
									Von_Datum 					date,
									Von_Tageszeit			TIME,  
									Bis_Datum					date,
									Bis_Tageszeit				TIME, 
									FOREIGN KEY (Tier_id)       	     REFERENCES Tier(Tier_id)
									FOREIGN KEY (Gehege_id)       REFERENCES  Gehege(Gehege_id)
);


DROP TABLE if EXISTS Gehege;
CREATE TABLE Gehege(
									Gehege_id 									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Gehegeart									VARCHAR (250),
									Groesseninheit							VARCHAR (20),
									Groesse											REAL,
									Bemerkungen								VARCHAR (250),
									Bezeichnung								VARCHAR (250)
);


DROP TABLE if EXISTS Strecke;
CREATE TABLE Strecke(
									Strecke_id 									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Bezeichnung								VARCHAR (50),   
									"Start"											INTEGER,
									Ende												INTEGER,
									Barrierefrei									VARCHAR (8),
									Bemerkungen								VARCHAR (250),
									FOREIGN KEY ("Start") REFERENCES Gehege(Gehege_id)
									FOREIGN KEY (Ende) REFERENCES Gehege(Gehege_id)
);


DROP TABLE if EXISTS RundwegStrecke;
CREATE TABLE RundwegStrecke(
									RundwegStrecke_id     							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Rundweg_id											INTEGER,
									Strecke_id  												INTEGER,
									FOREIGN KEY (Rundweg_id  )        REFERENCES Rundweg
									FOREIGN KEY (Strecke_id  )           REFERENCES Strecke
);


/*Leg3 */	
DROP TABLE if EXISTS Rundweg;
CREATE TABLE Rundweg(
									Rundweg_id     							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Bezeichnung								VARCHAR (60),
									"Entfernung_(Meter)"			    REAL,
									"Geh-Dauer_(Minute)"				REAL,
									Barrierefrei									VARCHAR (8),
									Bemerkungen								VARCHAR (250)
);


DROP TABLE if EXISTS GehegeGebaeude;
CREATE TABLE GehegeGebaeude(
									GehegeGebaeude_id 							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Gehege_id											INTEGER,
									Gebaeude_id										INTEGER,
									FOREIGN KEY (Gehege_id) 			REFERENCES Gehege
									FOREIGN KEY  (Gebaeude_id)  	REFERENCES Gebaeude
);


/*Leg3 */	
DROP TABLE if EXISTS Gebaeude;
CREATE TABLE Gebaeude(
									Gebaeude_id 									INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Bezeichnung									VARCHAR (250),
									Gelaendeposition					  	 	INTEGER, 
									Groessenheit							  	 	VARCHAR (20),
									Groesse												REAL,
									Bemerkungen								  	 VARCHAR (250)
);


DROP TABLE if EXISTS Historie;
CREATE TABLE Historie(
									Historie_id   					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Tier_id								INTEGER,
									Von 									date,
									Bis										date,
									"Wildpark/Zoo"				VARCHAR (40),   
									Mutter								VARCHAR (40), 
									Vater									VARCHAR (40), 
									Bemerkungen					VARCHAR (250),							
									FOREIGN KEY (Tier_id)       	  REFERENCES Tier(Tier_id)
);


DROP TABLE if EXISTS Tier;
CREATE TABLE Tier(
									Tier_id 								INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
									Tierart_id						INTEGER,
									Historie_id						INTEGER,
									Vater									INTEGER,
									Mutter								INTEGER,
									Tiername							VARCHAR (40),
									Geburtsdatum				date,
									ImZooGeboren				VARCHAR (8),
									ImZooSeit						date,
									Geschlecht						VARCHAR (8),
									Groesse								REAL,
									Groessenheit					VARCHAR (20),
									Gewicht                             REAL,
									Gewichtseinheit			    VARCHAR (20),
									Sterbedatum					date,
									Bemerkungen					 VARCHAR (250),
									FOREIGN KEY (Tierart_id)       	  REFERENCES Tierart(Tierart_id)
									FOREIGN KEY (Vater)					 REFERENCES    TIER(Tier_id)
									FOREIGN KEY (Mutter)				 REFERENCES    TIER(Tier_id)
);

DROP TABLE if EXISTS Vertraeglichkeit;
CREATE TABLE Vertraeglichkeit(
								Vertraeglichkeit_id			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Tier_id									INTEGER,
								Tier0										INTEGER,
								Tier1										INTEGER,
								Vertraeglich							VARCHAR (8),
								FOREIGN Key  (Tier0)	REFERENCES Tier(Tier_id)
								FOREIGN Key  (Tier1)	REFERENCES Tier(Tier_id)				
);

DROP TABLE if EXISTS RT_Anrede;
CREATE TABLE RT_Anrede(
								Anrede_id							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Anrede									VARCHAR (8)   
);			

DROP TABLE if EXISTS RT_Funktion;
CREATE TABLE RT_Funktion(
								Funktion_id							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Funktion								VARCHAR (40)   
);

DROP TABLE if EXISTS RT_Gewichtseinheit;
CREATE TABLE RT_Gewichtseinheit(
								Gewichtseinheit_id					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Gewichtseinheit							VARCHAR (20)
);
		
DROP TABLE if EXISTS RT_Groesseneinheit;
CREATE TABLE RT_Groesseneinheit(
								Groesseneinheit_id						INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Groesseneinheit								VARCHAR (10)
);			

DROP TABLE if EXISTS RT_Geschlecht;
CREATE TABLE RT_Geschlecht(
								Geschlecht_id						INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Geschlecht							VARCHAR (10)
);	

DROP TABLE if EXISTS RT_Mengeneinheit;
CREATE TABLE RT_Mengeneinheit(
								Mengeneinheit_id					INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Mengeneinheit						VARCHAR (20)
);		

DROP TABLE if EXISTS RT_Vertretungsgrund;
CREATE TABLE RT_Vertretungsgrund(
								Vertretungsgrund_id						INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Vertretungsgrund								VARCHAR (30)   
);

DROP TABLE if EXISTS RT_Titel;
CREATE TABLE RT_Titel(
								Titel_id							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Titel								VARCHAR (25)
);


DROP TABLE if EXISTS RT_Gehegeart;
CREATE TABLE RT_Gehegeart(
								Gehegeart_id							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								Bezeichnungen						VARCHAR (250)
);

DROP TABLE if EXISTS RT_PLZ_Ort;
CREATE TABLE RT_PLZ_Ort(
								PLZ_Ort_id							INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
								PLZ										VARCHAR (5),   
								Ort											VARCHAR (30)
);